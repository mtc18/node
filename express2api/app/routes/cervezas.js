const express = require('express')

// para establecer las distintas rutas, necesitamos instanciar el express router
var router = express.Router()
const cervezaController = require('../controllers/cervezaController.js')

// establecemos nuestra primera ruta, mediante get.

router.get('/', (req, res) =>{
  console.log('vamos a pedir lista  de cervezas')
  cervezaController.index(req, res)
})

router.get('/:id', (req, res) => {
  console.log('metodo SHOW  de cervezas')
  cervezaController.show(req, res)
  // res.json({ mensaje: `¡A beber cerveza numero ${req.params.id}!` })
})

router.delete('/:id', (req, res) => {
  console.log('metodo DELETE  de cervezas')
  cervezaController.destroy(req, res)
  // res.json({ mensaje: '¡Cerveza borrada!' })
})

router.post('/', (req, res) => {
  console.log('metodo CREATE  de cervezas')
  cervezaController.store(req, res)
  // res.json({ mensaje: `Cerveza creada: ${req.body.nombre}` })
})

router.put('/:id', (req, res) => {
  console.log('metodo UPDATE  de cervezas')
  cervezaController.update(req, res)
  // res.json({ mensaje: '¡Cerveza actualizada!' })
})

module.exports = router
