const express = require('express')

// para establecer las distintas rutas, necesitamos instanciar el express router
var router = express.Router()
const productController = require('../../controllers/v2/productController.js')
const auth=require('../../middlewares/auth')

router.use(auth.auth)

router.get('/', (req, res) =>{
  console.log('vamos a pedir lista  de PRODUCTOS')
  productController.index(req, res)
})


// router.get('/search', (req, res) =>{
//   console.log('vamos a BUSCAR lista  de PRODUCTOS')
//   productController.search(req, res)
// })

router.get('/:id', (req, res) =>{
  console.log('vamos a MOSTRAR un PRODUCTO')
  productController.show(req, res)
})

router.post('/', (req, res) =>{
  console.log('vamos a CREAR un PRODUCTO')
  productController.create(req, res)
})

router.put('/:id', (req, res) =>{
  console.log('vamos a ACTUALIZAR un PRODUCTO')
  productController.update(req, res)
})

router.delete('/:id', (req, res) => {
  console.log('metodo DELETE  de PRODUCTOS')
  productController.remove(req, res)
})

module.exports = router

