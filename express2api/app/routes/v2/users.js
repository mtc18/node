const express = require('express')
const router = express.Router()
const userController = require('../../controllers/v2/userController')

//registro de un nuevo usuario
router.post('/', userController.register)

router.get('/', (req, res) =>{
  console.log('vamos a pedir lista  de USERS')
  userController.index(req, res)
})

module.exports = router