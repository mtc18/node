const mongoose = require('mongoose')
const Schema = mongoose.Schema

const productSchema = new Schema({
  nombre: {
    type: String,
    maxlength: 20,
    required: true
  },
  precio:{
    type:Number,
    required: true
  },
  descripcion:{ 
    type:String,
    maxlength: 255
  },
  created: {
    type:Date,
    default:Date.now
  }
})

const Product = mongoose.model('Product', productSchema)

module.exports = Product

