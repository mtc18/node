const Order = require('../../models/v2/Order')
const User = require('../../models/v2/User')

const { ObjectId } = require('mongodb')

const index = (req, res) => {
  Order.find().populate('user_id').exec((err,order) => {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo el ORDER'
      })
    }
    return res.json(order)
  })
}

const show = (req, res) => {
  const id = req.params.id
  Order.findById(id).populate('user_id').
  exec(function (err, order){
    if(err){
      return res.status(500).json({
        message: 'Error'
      })
    }
    return res.json(order)
  });
}

const create = (req, res) => {
 const order= new Order(req.body)
 Order.create({
   user_id: order.user_id
 },
 function(err, order){
     if(err) {
       return res.json({error: err.errors.name})
     }
     return res.json(order)
   })
 }

const update = (req, res) => {
  const id = req.params.id
  Order.findOne({ _id: id }, (err, order) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al guardar el ORDER',
        error: err
      })
    }
    if (!order) {
      return res.status(404).json({
        message: 'No hemos encontrado el ORDER'
      })
    }

    Object.assign(order, req.body)

    order.save((err, order) => {
      if (err) {
        return res.status(500).json({
          message: 'Error al guardar el ORDER'
        })
      }
      if (!order) {
        return res.status(404).json({
          message: 'No hemos encontrado el ORDER'
        })
      }
      return res.json(order)
    })
  })
}

const remove = (req, res) => {
  const id = req.params.id
  Order.findOneAndDelete({ _id: id }, (err, order) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.json(500, {
        message: 'No hemos encontrado el ORDER'
      })
    }
    if (!order) {
      return res.status(404).json({
        message: 'No hemos encontrado el ORDER'
      })
    }
    return res.json(order)
  })
}

module.exports = {
  index,
  //search,
  show,
  create,
  update,
  remove
}
