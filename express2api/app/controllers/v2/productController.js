const Product = require('../../models/v2/Product')
const { ObjectId } = require('mongodb')

const index = (req, res) => {
  Product.find((err, products) => {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo el producto'
      })
    }
    return res.json(products)
  })
}

// const search = (req, res) => {//LA URL ES http://localhost:8080/api2/cervezas/search?q=alhambra (en vez de alhambra lo que sea)
//   const q = req.query.q
//   Product.find({ $text: { $search: q } }, (err, products) => {
//     if (err) {
//       return res.status(500).json({
//         message: 'Error en la búsqueda'
//       })
//     }
//     if (!products.length) {
//       return res.status(404).json({
//         message: 'No hemos encontrado PRODUCTOS que cumplan esa query'
//       })
//     } else {
//       return res.json(products)
//     }
//   })
// }


const show = (req, res) => {
  const id = req.params.id
  Product.findOne({ _id: id }, (err, product) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al obtener el PRODUCTO'
      })
    }
    if (!product) {
      return res.status(404).json({
        message: 'No tenemos este PRODUCTO'
      })
    }
    return res.json(product)
  })
}

const create = (req, res) => {
  const product = new Product(req.body)
  product.save((err, product) => {
    if (err) {
      return res.status(400).json({
        message: 'Error al guardar el PRODUCTO',
        error: err
      })
    }
    return res.status(201).json(product)
  })
}

const update = (req, res) => {
  const id = req.params.id
  Product.findOne({ _id: id }, (err, product) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al guardar el PRODUCTO',
        error: err
      })
    }
    if (!product) {
      return res.status(404).json({
        message: 'No hemos encontrado el PRODUCTO'
      })
    }

    Object.assign(product, req.body)

    product.save((err, product) => {
      if (err) {
        return res.status(500).json({
          message: 'Error al guardar el PRODCUTO'
        })
      }
      if (!product) {
        return res.status(404).json({
          message: 'No hemos encontrado el PRODUTO'
        })
      }
      return res.json(product)
    })
  })
}

const remove = (req, res) => {
  const id = req.params.id
  Product.findOneAndDelete({ _id: id }, (err, product) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.json(500, {
        message: 'No hemos encontrado el PRODUCTO'
      })
    }
    if (!product) {
      return res.status(404).json({
        message: 'No hemos encontrado el PRODUCTO'
      })
    }
    return res.json(product)
  })
}

module.exports = {
  index,
  //search,
  show,
  create,
  update, 
  remove
}