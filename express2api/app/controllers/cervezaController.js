const connection = require('../config/dbconnection.js')
const Cerveza = require('../models/Cerveza.js')

const index = (req, res) => {
  // res.json({ mensaje: `Lista de cervezas desde controller ` })
  // res.json({ mensaje: `Lista de cervezas, página ${page}` })

  const cervezas = Cerveza.index(function (status, data, fields) {
    res.status(status).json(data)
  })
}

const show = (req, res) => {
  const id = req.params.id
  Cerveza.find(id, (status, data) => {
    res.json(data)
  })
}

const store = (req, res) => {
  const cerveza = {
    name: req.body.name,
    container: req.body.container,
    price: req.body.price,
    alcohol: req.body.alcohol
  }
  Cerveza.create(cerveza, (err, data, fields) => {
    if (err) res.status(500).json({ mensaje: 'falló la inserción' })
    else res.status(status).json(data)
  })
}

const destroy = function(req, res) {
  const id = req.params.id
  Cerveza.destroy(id, (status, data) => {
    if(err){
      res.status(500).json('a la mierda')
    }
  })
}

const update = (req, res) => {
  const id = req.params.id
  const cerveza = {
    id: req.params.id,
    name: req.body.name,
    container: req.body.container,
    price: req.body.price,
    alcohol: req.body.alcohol
  }
  Cerveza.update(id, (status, data) => {
    res.json(data)
  })
}

module.exports = {
  index,
  show,
  store,
  destroy,
  update
}